// Axel Brochu, Samiuel Chowdhury, Mitchell Perusse, Shakela Hossain
// AI Name: Trinity++
class TrinityPlusPlus : IRPSAi
{

    public string GetChoice(Player us, Player opponent)
    {
        double totalCount = opponent.RockCount+ opponent.ScissorsCount+ opponent.PaperCount;
        if(totalCount <= 3)
        {
            //random
            Random rand = new Random();
            int choice = rand.Next(3);
            if (choice == 0)
            {
                return "rock";
            }
            else if (choice == 1)
            {
                return "paper";
            }
            else if (choice == 2)
            {
                return "scissors";
            }
            else
            {
                return "oops";
            }
        }
        else
        {
            int RockPercentage = (int) (opponent.RockCount / totalCount *100.0);
            int SciPercentage = (int) (opponent.ScissorsCount / totalCount *100.0);
            int PaperPercentage = (int) (opponent.PaperCount / totalCount *100.0);

            Random rand = new Random();
            int choice = rand.Next(100) + 1;

            if (choice > 0 && choice <= RockPercentage)
            {
                return "paper";
            }
            else if(choice > RockPercentage && choice <= SciPercentage + RockPercentage)
            {
                return "rock";
            }
            else
            {
                return "scissors";
            }
        }
        // Last pick
        // If we gotta get here, return scissors
        return "scissors";
    }
}